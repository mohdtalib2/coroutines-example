package com.ai.coroutinesandflows

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

class AsyncAwaitActivity : AppCompatActivity() {

    private lateinit var output1: TextView
    private lateinit var output2: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_async_await)

        output1 = findViewById(R.id.time1)
        output2 = findViewById(R.id.time2)

        findViewById<Button>(R.id.start).setOnClickListener {

            lifecycleScope.launch {
                while (true) {
                    delay(1000L)
                    Log.d("Number", "still running")
                }
            }

            GlobalScope.launch {
                delay(4000)
                val intent = Intent(this@AsyncAwaitActivity, CoroutineScopeActivity::class.java)
                startActivity(intent)
                finish()
            }


        }

        GlobalScope.launch(Dispatchers.IO) {

            val time = measureTimeMillis {

                val answer1 = networkCall1()

                val answer2 = networkCall2()


            }
            withContext(Dispatchers.Main) {
                Toast.makeText(this@AsyncAwaitActivity, "Done", Toast.LENGTH_SHORT).show()
                output1.text = (time / 1000).toString() + " second"
            }

        }


        GlobalScope.launch(Dispatchers.IO) {

            val time = measureTimeMillis {

                val answer1 = async { networkCall1() }

                val answer2 = async { networkCall2() }

                answer1.await()
                answer2.await()

            }
            withContext(Dispatchers.Main) {
                Toast.makeText(this@AsyncAwaitActivity, "Done", Toast.LENGTH_SHORT).show()
                output2.text = (time / 1000).toString() + " second"
            }

        }


    }


    private suspend fun networkCall1(): String {
        delay(3000L)
        return "Answer 1"
    }

    private suspend fun networkCall2(): String {
        delay(3000L)
        return "Answer 2"
    }

}