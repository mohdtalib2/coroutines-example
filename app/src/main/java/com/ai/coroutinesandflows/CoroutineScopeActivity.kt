package com.ai.coroutinesandflows

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class CoroutineScopeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutine_scope)
    }
}