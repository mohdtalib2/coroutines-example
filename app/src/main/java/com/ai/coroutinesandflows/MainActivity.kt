package com.ai.coroutinesandflows

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {

    private lateinit var text1: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text1 = findViewById(R.id.text1)

        runBlocking {
            delay(2000)
            text1.text = "Waiting for output"
        }

      GlobalScope.launch(Dispatchers.IO) {
          withTimeout(4000) {
              val result1 = doNetworkCall()
              withContext(Dispatchers.Main) {
                  text1.text = result1
              }
          }

          val job = launch {
              for(i in 1..5) {
                  Log.d("Number", "Number = $i")
              }
          }

          job.join()

      }

    }

    private suspend fun doNetworkCall(): String {
        delay(3000)
        return "First network Call done"

    }

}